//
// Quote
// --------------------------------------------------

var total_mex_proces = 0;
var total_usa_proces = 0;

var alert_1 = false;
var alert_2 = false;
var alert_3 = false;
// var alert_4 = false;

var occlusal_selected = false;
var ctscan_selected = false;

function reset_promotions(){
    alert_1 = false;
    alert_2 = false;
    alert_3 = false;
    // alert_4 = false;
}

Number.prototype.formatMoney = function(c, d, t){
    var n = this;
    c = (isNaN(c = Math.abs(c)) ? 2 : c);
    d = (d === undefined ? "." : d);
    t = (t === undefined ? "," : t);
    var s = (n < 0 ? "-" : "");
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "";
    var j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t);
};

function reverse(str){
    return str.split("").reverse().join("");
}

//function sum_prices(){
//    $("table.quote-tabular > tbody > tr").each(function(){
//        var points = $(this).find('.quantity').children('input');
//    });
//
//    //refresh prices
//    refresh_proces();
//}

function clear_alert_boxes() {
    $('.alert-box').each(function(){
        $(this).find('.close').trigger('click');
    });
}

function refresh_proces(){
    var time = 1200;
    var porcent = (100 - (parseInt(total_mex_proces / (total_usa_proces || 1) * 100, 10) || 100));

    $('#our-cost').find('span').text('$' + (total_mex_proces).formatMoney());
    $('#us-cost').find('span').text('$' + (total_usa_proces).formatMoney());
    $('.porcent').text(porcent + '%');
    $('.total-difference').text('$' + (total_usa_proces - total_mex_proces).formatMoney());

//    if(total_mex_proces === 0){
//        reachSlide.showSlider();
//    } else if(total_mex_proces < 999 && !alert_1){
//        reachSlide.hideSlider();
//        $("#slideinner-content").html($('#alert-1').html());
//        setTimeout(function(){
//            reachSlide.showSlider();
//        }, time);
//        reset_promotions();
//        alert_1 = true;
//    } else if (total_mex_proces >= 1000 && total_mex_proces < 2000 && !alert_2){
//        reachSlide.hideSlider();
//        $("#slideinner-content").html($('#alert-2').html());
//        setTimeout(function(){
//            reachSlide.showSlider();
//        }, time);
//        reset_promotions();
//        alert_2 = true;
//    } else if (total_mex_proces >= 2000 && total_mex_proces < 4000 && !alert_3) {
//        reachSlide.hideSlider();
//        $("#slideinner-content").html($('#alert-3').html());
//        setTimeout(function(){
//            reachSlide.showSlider();
//        }, time);
//        reset_promotions();
//        alert_3 = true;
//    }

//     Active Alert 4
//     else if (total_mex_proces >= 4000 && !alert_4) {
//         reachSlide.hideSlider();
//         $("#slideinner-content").html($('#alert-4').html());
//         setTimeout(function(){
//             reachSlide.showSlider();
//         }, time);
//         reset_promotions();
//         alert_4 = true;
//     }

//    $('#btnClose').click(function () {
//        reachSlide.hideSlider();
//    });
}

//If user clicks to close button, setting IsPopupClosed variable to true
//Show that popup should not be visible on scroll up or down
var IsPopupClosed = false;
//var reachSlide = {
//    showSlider: function () {
//        var container = $("#slidecontainer");
//        if (!IsPopupClosed) {
//            if (!container.is(":visible")) {
//                // Show - slide down.
//                container.slideDown(300);
//            }
//        }
//    },
//    hideSlider: function () {
//        var container = $("#slidecontainer");
//        // visibility.
//        if (container.is(":visible")) {
//            // Hide - slide up.
//            container.slideUp(300);
//        }
//    }
//};

function clear_num(str) {
    return parseInt(str.replace(/[A-Za-z$-,---]/g, ""), 10);
}

//reachSlide.hideSlider();

$(document).ready(function() {
    var prices = $("#prices-div");

    if (prices.length) {
    var fixTop = prices.offset().top;
    var last_price = $("#last-price").offset().top + $("#last-price").height();

    $(window).scroll(function(){
        if($(document).width() > 1110) {
            var scroll_top = $(window).scrollTop();
            var prices_pos = $(".content-all").width() - $("#last-price").width();

            if(scroll_top >= fixTop){
                $("#prices-div").addClass('fixed-top');
            } else if(scroll_top < fixTop) {
                $("#prices-div").removeClass('fixed-top');
            }

            if(scroll_top + $("#prices-div").height() >= last_price){
                $("#prices-div").css({
                    'top': last_price - fixTop - $("#prices-div").height() + 'px',
                    'position': 'absolute'
                });
            } else {
                $("#prices-div").attr('style', '');
            }
        }
    });

    $(".procedure-box").each(function(){
        var self = $(this);
        var points = self.find('.quantity').children('input');

        var mex_price = clear_num(self.find('.our-quote-price').text());
        var usa_price = clear_num(self.find('.us-quote-price').text());

        points.val(String(0));
        points.attr('readonly', 'readonly');

        self.find('.minus').on('click', function(){
            if(parseInt(points.val(), 10) > 0){
                points.val(String(parseInt(points.val(), 10) - 1));

                total_mex_proces -= mex_price;
                total_usa_proces -= usa_price;

                if($(this).parent().parent().find(".occlusal").length > 0) {
                    occlusal_selected = false;

                    $('.occlusal').each(function() {
                        var quantity = $(this).parent().find(".quantity > input").val();
                        if(quantity !== "" && quantity !== "0") {
                            occlusal_selected = true;
                        }
                    });

                    if(!occlusal_selected) {
                        $('.occlusal-procedure').parent().find(".quantity > .minus").attr('disabled', false);
                        $('.occlusal-procedure').parent().find(".quantity > .plus").attr('disabled', false);
                        $('.occlusal-procedure').parent().find(".quantity > .minus").trigger("click");
                    }
                }

                if($(this).parent().parent().find(".ct-scan").length > 0) {
                    ctscan_selected = false;

                    $('.ct-scan').each(function() {
                        var quantity = $(this).parent().find(".quantity > input").val();
                        if(quantity !== "" && quantity !== "0") {
                            ctscan_selected = true;
                        }
                    });

                    if(!ctscan_selected) {
                        $('.ct-scan-procedure').parent().find(".quantity > .minus").attr('disabled', false);
                        $('.ct-scan-procedure').parent().find(".quantity > .plus").attr('disabled', false);
                        $('.ct-scan-procedure').parent().find(".quantity > .minus").trigger("click");
                    }
                }

                refresh_proces();
            }
        });

        self.find('.plus').on('click', function(){
            points.val(String(parseInt(points.val(), 10) + 1));

            total_mex_proces += mex_price;
            total_usa_proces += usa_price;

            if($(this).parent().parent().find(".occlusal").length > 0 && !occlusal_selected) {
                occlusal_selected = true;
                $('.occlusal-procedure').parent().find(".quantity > input").val("0");
                $('.occlusal-procedure').parent().find(".quantity > .plus").trigger("click");
                $('.occlusal-procedure').parent().find(".quantity > .minus").attr('disabled', true);
                $('.occlusal-procedure').parent().find(".quantity > .plus").attr('disabled', true);
            }

            if($(this).parent().parent().find(".ct-scan").length > 0 && !ctscan_selected) {
                ctscan_selected = true;
                $('.ct-scan-procedure').parent().find(".quantity > input").val("0");
                $('.ct-scan-procedure').parent().find(".quantity > .plus").trigger("click");
                $('.ct-scan-procedure').parent().find(".quantity > .minus").attr('disabled', true);
                $('.ct-scan-procedure').parent().find(".quantity > .plus").attr('disabled', true);
            }

            refresh_proces();
        });
    });

    $("#free-quote-clear").on('click', function(){
        $("div.large-6.columns").each(function(){
            $(this).find('.quantity').find('input').val('0');

//            reachSlide.hideSlider();

            total_mex_proces = 0;
            total_usa_proces = 0;

            refresh_proces();
        });
    });

    function send_effect() {
        $(".loading-gif").css('display', 'inline-block');
        $(".form-buttons").css('display', 'none');
    }

    function cancel_send_effect() {
        $(".loading-gif").css('display', 'none');
        $(".form-buttons").css('display', 'inline-block');
    }

    $("#free-quote-form").on('submit', function(event) {
        event.preventDefault();

        send_effect();

        var procedures = [];
//        console.log()
        $(".procedure-box").each(function(){
            var points = $(this).find('.quantity').find('input');

            if(points.val() !== '0' && points.val() !== '' && points.val() !== undefined){
                procedures.push({
                    id: $(this).find('.procedure').attr('data-id'),
                    price: clear_num($(this).find('.our-quote-price').text()),
                    quantity: points.val()
                });
            }
        });

        if(procedures.length > 0) {
            var pro_str = "";
            for(var i = 0; i < procedures.length; i++){
                var p = procedures[i];
                pro_str += '' + p.id + ':' + p.quantity + (i + 1 < procedures.length ? ',' : '');
            }

            $('input[name="procedures"]').val(pro_str);
        }

        $('input[name="price"]').val($('.large-6.columns.our-cost').find('span').text());

        if(total_mex_proces === 0){
            alert('Error, you need to select a procedure');
            cancel_send_effect();
        }
        else {
            document.getElementById("free-quote-form").submit();
//            $(this).ajaxSubmit({
//                success: function(){
//                    window.location.href = "/thanks-free-quote/";
//                },
//                error: function(){
//                    cancel_send_effect();
//                    alert('Error, sending your quote!');
//                }
//            });
        }

        return false;
    });
}
});
