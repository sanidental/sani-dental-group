//
// Main
// --------------------------------------------------

window.$ = window.jQuery = require('jquery');
require('foundation-sites');
import 'jquery-ui-bundle';

$(document).foundation();

$(function() {
// Number fields
	$('#phone-number-field').keyup(function(){
	  $(this).val($(this).val().replace(/(\d{3})\-?(\d{4})/,'$1-$2'))
	});

	// Contact Form [Acquaintance Name]
	$('#find-list').bind('change', function (e) {
	  if( $('#find-list').val() == "acquaintance") {
	    $('#acquaintance-name').show();
	  }
	  else{
	    $('#acquaintance-name').hide();
	  }
	});

	$( "#dateForm" ).datepicker({
		dateFormat : 'yy-mm-dd',
		changeMonth : true,
		changeYear : true,
		yearRange: "-90:+1"
	});

});