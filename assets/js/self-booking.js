//
// Self booking
// --------------------------------------------------
import Vue from 'vue';
import DatePicker from './components/DatePicker.vue';
// Vue.component('Example', require('./Example.vue').default);

const Treatments = [
  {
    id: 1,
    name: 'Diagnosis'
  },
  {
    id: 2,
    name: 'All on 4'
  },
  {
    id: 3,
    name: 'Bone Graft'
  },
  {
    id: 4,
    name: 'Crown'
  },
  {
    id: 5,
    name: 'Denture'
  },
  {
    id: 6,
    name: 'Extraction'
  },
  {
    id: 7,
    name: 'Filling'
  },
  {
    id: 8,
    name: 'Implant'
  },
  {
    id: 9,
    name: 'Oral Surgery'
  },
  {
    id: 10,
    name: 'Root Canal'
  },
  {
    id: 11,
    name: 'Sinus Lift'
  },
  {
    id: 12,
    name: 'Teeth Whitening'
  },
  {
    id: 13,
    name: 'Teeth Cleaning'
  }
];

// <date-picker @update-date="updateDate"></date-picker>

new Vue({
  delimiters: ['${', '}'],
  el: '#app',
  components: {
    DatePicker
  },
  data: {
    currentStep: 1,
    date: '',
    time: '',
    treatment: [],
    message: '',
    firstName: '',
    lastName: '',
    location: '',
    email: '',
    phone: '',
    promoCode: '',
    treatments: Treatments
  },
  filters: {
    spacing(value) {
      if (!value) return '';
      return value.join(', ');
    }
  },
  mounted() {
    $(this.$el).foundation();
  },
  destroyed() {
    $(this.$el).foundation.destroy();
  },
  methods: {
    goToStep(step) {
      this.currentStep = step;
    },
    updateDate(date) {
      this.date = date;
    }
  },
  computed: {
    step_one() {
      return this.firstName && this.lastName && this.location && this.phone && this.email ? true : false;
    },
    step_two() {
      return this.date && this.time && this.treatment && this.message ? true : false;
    },
    step_three() {
      return this.step_one && this.step_two ? true : false;
    }
  }
});