//
// Reviews
// --------------------------------------------------

$(function() {
	var inputs = [];
	var count = 0;

	function changeStars($this, e, active, input){
		var x =  e.pageX - $this.offset().left;
		if (active) {
			var start = 0;
			if(x > 7){
					start = parseInt((x + 20) / 20, 10) * 20;
			}

			console.log(start);

			$this.find('.star-rating.write').css({
					'width': start
			});
			input.val((start / 10) / 2);
		}
	}

	$('.qualification').each(function(){
		inputs.push($(this));
	});

	$('.star-holder').each(function(){
		var active = false;
		var input = inputs[count++];

		$(this).on('mousemove', function(e){
				changeStars($(this), e, active, input);
		});

		$(this).on('mousedown', function(e){
				active = true;
				changeStars($(this), e, active, input);
		});

		$(this).on('mouseup', function(){
				active = false;
		});
	});

});




