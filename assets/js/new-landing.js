import Vue from 'vue'
window.axios = require('axios');
import Vuelidate from 'vuelidate'
import { required, email, minLength } from 'vuelidate/lib/validators'
import '../scss/new-landing.sass'

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json' 
}

let urlForm = 'https://api.forms.sanimedical.info/v1/send/sani_ySEWnbR59plx5QCwDNFphcir'

Vue.use(Vuelidate)

const Treatments = [
  {
    id: 1,
    name: 'Initial Examination / Consultation',
    price: 'Free'
  },
  {
    id: 2,
    name: 'Dental Crowns starting at',
    price: '180'
  },
  {
    id: 3,
    name: 'Dentures starting at',
    price: '250'
  },
  {
    id: 4,
    name: 'Titanium Dental Implant (implant only) starting at',
    price: '750'
  },
  {
    id: 5,
    name: 'Implant Crown (including abutment) starting at',
    price: '470'
  },
  {
    id: 6,
    name: 'Bone graft (per unit)',
    price: '400'
  },
  {
    id: 7,
    name: 'Sinus Lifting starting at',
    price: '700'
  },
  {
    id: 8,
    name: 'Root canal (any tooth)',
    price: '220'
  },
  {
    id: 9,
    name: 'Extraction (simple)',
    price: '48'
  },
  {
    id: 10,
    name: 'Deep Cleaning, Scaling & Root Planing (per quadrant)',
    price: '60'
  },
  {
    id: 11,
    name: 'LANAP (per quadrant)',
    price: '150'
  },
  {
    id: 12,
    name: 'Teeth Whitening, starting at',
    price: '170'
  }
]

new Vue({
  delimiters: ['${', '}'],
  el: '#app',
  data: {
    treatments: Treatments,
    formData: {
      name: '',
      email: '',
      phone: '',
      message: '',
      field_utm_landing: '',
      source: ''
    },
    submitStatus: null
  },
  validations: {
    formData: {
      name: { required, minLength: minLength(4) },
      email: { required, email },
      message: { required, minLength: minLength(10) }
    }
  },
  methods: {
    submit() {
      this.formData.field_utm_landing = this.$refs.utm.value
      this.formData.source = this.$refs.source.value
      // console.log(this.formData)
      this.$v.$touch()
      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR'
      } else {
        this.submitStatus = 'PENDING'
        axios.post(urlForm, this.formData)
          .then(res => {
            this.submitStatus = 'OK'
            window.location.replace('/landing-thanks/')
          })
          .catch(err => {
            this.submitStatus = 'ERROR'
            console.error(err)
          })
      }
    }
  }
})