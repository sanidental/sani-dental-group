from django import forms
from responses.models import Response


class ResponseForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea(attrs={'class': 'mceEditor'}))

    class Meta:
        model = Response
        fields = '__all__'