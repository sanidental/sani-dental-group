from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from mezzanine.core.admin import SitePermissionInline
from .models import User


class SitePermissionInline(TabularDynamicInlineAdmin):
    model = SitePermissionInline.model
    max_num = 1
    can_delete = False


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {
            'fields': ('username', 'password')
        }),
        (('Personal info'), {
            'fields': ('first_name', 'last_name', 'email')
        }),
        (('Profile'), {
            'fields': ('photo', 'biography', 'google_plus')
        }),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser',
                                    'groups', 'user_permissions')
        }),
        (('Important dates'), {
            'fields': ('last_login', 'date_joined')
        }),
    )
    inlines = (SitePermissionInline, )

admin.site.register(User, CustomUserAdmin)
