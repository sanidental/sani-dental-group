from django import template
from reviews.models import Review
register = template.Library()


@register.inclusion_tag('reviews/list.html')
def render_reviews():
    return {'reviews': Review.objects.all()}


@register.simple_tag
def calc_stars(cal):
    return "%dpx" % (cal * 2 * 10);