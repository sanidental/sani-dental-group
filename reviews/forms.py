from django import forms
from .models import (Video, Gallery,
                     BeforeAndAfter)


class VideoForm(forms.ModelForm):
    url = forms.CharField(label='URL')

    class Meta:
        model = Video
        fields = ('url',)


class GalleryForm(forms.ModelForm):
    photo = forms.FileField(label='Photo')

    class Meta:
        model = Gallery
        fields = ('photo',)


class BeforeAfterForm(forms.ModelForm):
    before = forms.FileField(label='Before')
    after = forms.FileField(label='After')
    date = forms.DateField(label='Date')
    procedure = forms.ComboField(label='Procedure')

    class Meta:
        model = BeforeAndAfter
        fields = ('before', 'after', 'date',
                  'procedure')
