from django.contrib import admin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import Review, VideoReview, Video, Gallery, BeforeAndAfter, ClinicalCase
from .forms import (VideoForm, GalleryForm,)


class VideoInline(TabularDynamicInlineAdmin):
    model = Video
    form = VideoForm


class GalleryInline(TabularDynamicInlineAdmin):
    model = Gallery
    form = GalleryForm


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'enable', 'created', 'quality',
                    'service', 'communication', 'cleanliness',
                    'price', 'comfort', 'value', 'location',
                    'satisfaction')
    list_editable = ('enable',)


class VideoReviewAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'enable',  'date', 'city',
                    'country', 'comment', 'url')
    list_editable = ('enable',)


class ClinicalCaseAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'enable', 'photo', 'excerpt',)
    filter_horizontal = ('before_and_after', 'group',)
    readonly_fields = ['slug', ]
    inlines = (VideoInline, GalleryInline,)
    list_editable = ('enable',)


class BeforeAndAfterAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'enable', 'date', 'before',
                    'after','city', 'country',
                    )
    filter_horizontal = ('procedure',)
    list_editable = ('enable',)


admin.site.register(Review, ReviewAdmin)
admin.site.register(VideoReview, VideoReviewAdmin)
admin.site.register(ClinicalCase, ClinicalCaseAdmin)
admin.site.register(BeforeAndAfter, BeforeAndAfterAdmin)
