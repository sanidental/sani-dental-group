from django.conf.urls import url
from .views import (DetailReview, Reviews, BeforeAfterByGroupView,
                    ClinicalCaseDetailView, VideoReviewListView,
                    GroupUrlListView,
                    ClinicalCasesUrlListView, VideoReviewDetailView)
from prices.views import  TreatmentDetail

urlpatterns = [
    url(r'^reviews/$', Reviews.as_view(), name="reviews"),
    url(r'^review/(?P<pk>\d+)/$', DetailReview.as_view(), name="review"),
    url(r'^before-and-after/$', GroupUrlListView.as_view(), name="list"),
    url(r'^before-and-after/(?P<slug>[-\w]+)/$', BeforeAfterByGroupView.as_view(), name="list-by-group"),
    url(r'^clinical-cases/(?P<slug>[-\w]+)/$', ClinicalCaseDetailView.as_view(), name="slug"),
    url(r'^clinical-cases/$', ClinicalCasesUrlListView.as_view(), name="clinical_case"),
    url(r'^video-reviews/$', VideoReviewListView.as_view(), name="video-review"),
    url(r'^video-reviews/(?P<pk>\d+)/$', VideoReviewDetailView.as_view(), name="video-review"),
    url(r'^patient-resources/treatments/(?P<slug>[-\w]+)/$', TreatmentDetail.as_view(), name="treatment "),
]
