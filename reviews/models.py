from django.db import models
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from prices.models import Procedure, Group
import unicodedata

CLINICS = (
    'Sani Dental Group',
    'Sani Dental Group Platinum',
    'Sani Dental Group Class'
)

CHOICES_CLINICS = zip(CLINICS, CLINICS)


class Review(models.Model):
    enable = models.BooleanField(default=False, blank=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField()
    date = models.DateField()
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    find_us = models.CharField(max_length=255)
    acquaintance = models.CharField(max_length=255, null=True, blank=True)
    comment = models.TextField()
    quality = models.IntegerField(default=5)
    service = models.IntegerField(default=5)
    communication = models.IntegerField(default=5)
    cleanliness = models.IntegerField(default=5)
    price = models.IntegerField(default=5)
    comfort = models.IntegerField(default=5)
    value = models.IntegerField(default=5)
    location = models.IntegerField(default=5)
    satisfaction = models.IntegerField(default=5)

    clinic = models.CharField(max_length=100, choices=CHOICES_CLINICS)

    question_1 = models.BooleanField(
        verbose_name=u'Was your diagnosis consultation on time')
    question_2 = models.BooleanField(
        verbose_name=u'Were your following appointments on time')

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)

    def full_name(self):
        return u"%s %s" % (self.first_name, self.last_name)

    def __unicode__(self):
        return self.full_name()

    def average(self):
        addition = sum((
            self.quality, self.service, self.communication,
            self.cleanliness, self.price, self.comfort, self.value,
            self.location, self.satisfaction))
        return int(round(addition / 9))

    def get_absolute_url(self):
        return reverse('review', kwargs={'pk': self.pk})


class VideoReview(models.Model):
    enable = models.BooleanField(default=False, blank=True)
    full_name = models.CharField(max_length=255)
    date = models.DateField()
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    comment = models.TextField()
    url = models.URLField(max_length=255)

    class Meta:
        ordering = ('-date',)


class ClinicalCase(models.Model):
    enable = models.BooleanField(default=False, blank=True)
    photo = models.ImageField(upload_to='reviews/clinical-case/photo', blank=True, null=True)
    excerpt = models.TextField()
    full_name = models.CharField(max_length=255)
    group = models.ManyToManyField('prices.Group', blank=True)
    before_and_after = models.ManyToManyField('BeforeAndAfter', blank=True)
    slug = models.SlugField(max_length=255)
    date = models.DateField(default=None)
    page_size = 15
    page_number = 1

    class Meta:
        ordering = ('-date',)

    def __unicode__(self):
        return self.full_name

    def __init__(self, *args, **kwargs):
        super(ClinicalCase, self).__init__(*args, **kwargs)
        if self.full_name is not None:
            self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.full_name)
                                ) if unicodedata.category(c) != 'Mn'))

    def save(self, *args, **kwargs):
        self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.full_name)
                                ) if unicodedata.category(c) != 'Mn'))
        super(ClinicalCase, self).save(*args, **kwargs)

    def before_afters(self):
        return BeforeAndAfter.objects.filter(clinicalcase=self)

    def videos(self):
        return Video.objects.filter(clinical_case=self)

    def galleries(self):
        return Gallery.objects.filter(clinical_case=self)

    def group_before_after(self):
        return Group.objects.filter(procedure__beforeandafter__clinicalcase__slug=self.slug).distinct()

    def groups(self):
        return Group.objects.filter(clinicalcase=self)


class BeforeAndAfter(models.Model):
    enable = models.BooleanField(default=False, blank=True)
    full_name = models.CharField(max_length=255, default="", )
    city = models.CharField(max_length=255, default='',)
    country = models.CharField(max_length=255, default='',)
    date = models.DateField()
    procedure = models.ManyToManyField('prices.Procedure', blank=True)
    before = models.ImageField(upload_to='reviews/clinical-case/before',
                               blank=True, null=True)
    after = models.ImageField(upload_to='reviews/clinical-case/after',
                              blank=True, null=True)
    page_size = 15
    page_number = 1

    class Meta:
        verbose_name_plural = 'Before And After'
        ordering = ('-date',)

    def __unicode__(self):
        date_format = "%Y-%m-%d"
        return self.full_name+ self.date.strftime(" %s " % (date_format,))

    def procedures(self):
        return Procedure.objects.filter(beforeandafter=self)

    def groups(self):
        return Group.objects.filter(procedure__beforeandafter=self).distinct()


class Gallery(models.Model):
    clinical_case = models.ForeignKey('ClinicalCase')
    photo = models.ImageField(upload_to='reviews/clinical-case/gallery',
                              blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Gallery'


class Video(models.Model):
    clinical_case = models.ForeignKey('ClinicalCase')
    url = models.URLField(max_length=255)

    class Meta:
        verbose_name_plural = 'Videos'

