from django.views.generic import DetailView, ListView
from django.http import Http404
from mezzanine.pages.models import Page
from .models import Review, BeforeAndAfter, ClinicalCase, VideoReview
from prices.models import Group


class DetailReview(DetailView):
    template_name = "reviews/detail.html"
    context_object_name = "review"
    model = Review

    def get_context_data(self, **kwargs):
        context = super(DetailReview, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="reviews")
        except Page.DoesNotExist:
            raise Http404
        return context


class Reviews(ListView):
    template_name = "reviews/list.html"
    context_object_name = "reviews"
    queryset = Review.objects.filter(enable=True)
    paginate_by = 25

    def get_context_data(self, **kwargs):
        context = super(Reviews, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="reviews")
        except Page.DoesNotExist:
            raise Http404
        context['reviews_number'] = Page.objects.all().count()
        return context


class BeforeAfterByGroupView(DetailView):
    template_name = "reviews/before-and-after/list_by_group.html"

    def get_object(self, queryset=None):
        return None

    def get_context_data(self, **kwargs):
        context = super(BeforeAfterByGroupView, self).get_context_data(**kwargs)
        try:
            page_number = int(self.request.GET.get('page', '1'))
            rows = BeforeAndAfter.objects.filter(procedure__group__slug=self.kwargs['slug']).filter(enable=True).distinct().count()
            if rows > 0:
                number_pages = range(1, (abs(rows - 1) / BeforeAndAfter.page_size) + 2)
            else:
                number_pages = ''
            if 1 > page_number or page_number > len(number_pages) and rows > 0:
                raise Http404
            initial_row = BeforeAndAfter.page_size * (page_number - 1)
            final_row = page_number * ClinicalCase.page_size
            context["group"] = Group.objects.get(slug=self.kwargs['slug'])
            context["before_and_afters"] = BeforeAndAfter.objects.filter(procedure__group__slug=self.kwargs['slug']
                                                                         ).filter(enable=True).distinct()[initial_row:final_row]
            context["page"] = Page.objects.get(slug="before-and-after")
            context["url"] = "/before-and-after/"+self.kwargs['slug']+"/?page="
            context["pagination"] = {
                                    'previous_page': page_number > 1,
                                    'next_page': page_number < len(number_pages),
                                    'current_page': page_number,
                                    'number_pages': number_pages
                                    }
        except Page.DoesNotExist:
            raise Http404
        return context


class ClinicalCaseDetailView(DetailView):
    template_name = "reviews/clinical-cases/detail.html"
    context_object_name = "clinical_cases"
    model = ClinicalCase

    def get_queryset(self):
        queryset = ClinicalCase.objects.filter(slug=self.kwargs['slug']).filter(enable=True)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ClinicalCaseDetailView, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="clinical-cases")
            context["page"].title = ''
        except Page.DoesNotExist:
            raise Http404
        return context


class ClinicalCasesUrlListView(DetailView):
    template_name = "reviews/clinical-cases/list.html"

    def get_object(self, queryset=None):
        return None

    def get_context_data(self, **kwargs):
        context = super(ClinicalCasesUrlListView, self).get_context_data(**kwargs)
        try:
            page_number = int(self.request.GET.get('page', '1'))
            rows = ClinicalCase.objects.filter(enable=True).count()
            if rows > 0:
                number_pages = range(1, abs(rows - 1) / ClinicalCase.page_size + 2)
            else:
                number_pages = ''
            if 1 > page_number or page_number > len(number_pages) and rows > 0:
                raise Http404
            initial_row = ClinicalCase.page_size * (page_number - 1)
            final_row = page_number * ClinicalCase.page_size
            context["page"] = Page.objects.get(slug="clinical-cases")
            context["clinical_cases"] = ClinicalCase.objects.filter(enable=True)[initial_row:final_row]
            context["url"] = "/clinical-cases/?page="
            context["pagination"] = {
                                    'previous_page': page_number > 1,
                                    'next_page': page_number < len(number_pages),
                                    'current_page': page_number,
                                    'number_pages': number_pages
                                    }
        except Page.DoesNotExist:
            raise Http404
        return context


class VideoReviewListView(ListView):
    template_name = "reviews/video-reviews/list.html"
    context_object_name = "video_reviews"
    paginate_by = 15
    model = VideoReview
    queryset = VideoReview.objects.filter(enable=True)

    def get_context_data(self, **kwargs):
        context = super(VideoReviewListView, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="video-reviews")
        except Page.DoesNotExist:
            raise Http404
        return context


class VideoReviewDetailView(DetailView):
    template_name = "reviews/video-reviews/detail.html"
    context_object_name = "video_review"
    model = VideoReview

    def get_queryset(self):
        queryset = VideoReview.objects.filter(id=self.kwargs['pk']).filter(enable=True)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(VideoReviewDetailView, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="video-reviews")
        except Page.DoesNotExist:
            raise Http404
        return context


class GroupUrlListView(ListView):
    template_name = "reviews/before-and-after/group-list.html"
    context_object_name = "groups"
    paginate_by = 15
    model = Group
    queryset = Group.objects.all()

    def get_context_data(self, **kwargs):
        context = super(GroupUrlListView, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="before-and-after")
        except Page.DoesNotExist:
            raise Http404
        return context

