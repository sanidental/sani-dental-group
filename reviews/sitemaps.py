from django.contrib.sitemaps import Sitemap
from .models import Review


class ReviewsSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Review.objects.filter(enable=True)

    def lastmod(self, obj):
        return obj.created
