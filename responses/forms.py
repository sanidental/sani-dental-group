from django import forms
from .models import Response


class ResponseForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea(attrs={'class': 'mceEditor'}), required=False)

    class Meta:
        model = Response
        fields = '__all__'