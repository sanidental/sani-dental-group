from django.test import LiveServerTestCase
from selenium import webdriver
import time

SITE_ROOT = "http://localhost:8081"


class SeleniumTestCase(LiveServerTestCase):
    fixtures = ('sani.json',)

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.maximize_window()
        self.browser.implicitly_wait(5)

    def test_1_link_in_homepage(self):
        self.browser.get(SITE_ROOT)
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('SANI DENTAL GROUP', body.text)

        self.browser.find_element_by_link_text('VIEW MORE').click()
        title = self.browser.find_elements_by_class_name('section-title')[0]
        self.assertIn("About SANI Dental Group", title.text)
        self.browser.back()

        self.browser.find_element_by_link_text('Learn more and get a free quote...').click()
        container = self.browser.find_element_by_class_name('slider-container')
        self.assertIn("Looking for a full mouth reconstruction?", container.text)
        self.browser.back()

        self.browser.find_element_by_link_text('MAKE AN APPOINTMENT').click()
        title = self.browser.find_element_by_class_name('section-title')
        self.assertIn("Set Up Your Appointment", title.text)
        self.browser.back()

    def test_2_appointments_side_form(self):
        self.browser.get(SITE_ROOT)
        side = self.browser.find_element_by_id('appointments-side-form')
        side.find_element_by_name('name').send_keys('Jesus Anaya')
        side.find_element_by_name('email').send_keys('jesus.anaya.dev@gmail.com')
        side.find_element_by_name('location').send_keys('Mexicali')

        element = side.find_element_by_name('gender')
        element.find_elements_by_tag_name('option')[1].click()

        element = side.find_element_by_name('find_us')
        element.find_elements_by_tag_name('option')[2].click()

        side.find_element_by_name('birthday').send_keys('10/05/1989')
        side.find_element_by_name('number').send_keys('6861112233')
        side.find_element_by_name('promo_code').send_keys('PROMO-CODE')
        side.find_element_by_name('message').send_keys('Test message 1234567890.')
        side.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(2)

        title = self.browser.find_element_by_class_name('section-title')
        self.assertIn("Thank You, your email has been sent.", title.text)
        self.browser.back()

    def tearDown(self):
        time.sleep(3)
        self.browser.quit()
