from django.contrib import admin
from mezzanine.blog.admin import BlogPostAdmin, BlogCategoryAdmin
from mezzanine.blog.models import BlogPost, BlogCategory
from copy import deepcopy


blog_fieldsets = deepcopy(BlogPostAdmin.fieldsets)
blog_fieldsets[0][1]["fields"].insert(2, "user")


class SaniBlogAdmin(BlogPostAdmin):
    fieldsets = blog_fieldsets


class SaniBlogCategoryAdmin(BlogCategoryAdmin):
    fieldsets = ((None, {"fields": ("title", "slug")}),)


admin.site.unregister(BlogPost)
admin.site.unregister(BlogCategory)
admin.site.register(BlogPost, SaniBlogAdmin)
admin.site.register(BlogCategory, SaniBlogCategoryAdmin)
