from django.conf.urls import url
from mezzanine.blog.views import blog_post_list

urlpatterns = [
    url(r"^blog/category/$", blog_post_list, name="blog_post_list_category"),
]
