from django.contrib.sitemaps import Sitemap
from mezzanine.blog.models import BlogCategory


class BlogCategorySitemap(Sitemap):
    changefreq = "never"
    priority = 0.3

    def items(self):
        return BlogCategory.objects.all()
