from django.contrib import admin
from admin_exporter.actions import export_as_csv_action
from admin_exporter.actions import export_as_json_action
from admin_exporter.actions import export_as_xml_action
from admin_exporter.actions import export_as_yaml_action

from .models import (NewsLetter, Feedback, Appointment, Contact, Popup,
                    LandingContact, Career, Position)


class AppointmentAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'date', 'time', 'number',
            'email', 'location', 'gender', 'birthday', 'find_us',
            'acquaintance_name', 'promo_code', 'message')


class ContactAdmin(admin.ModelAdmin):
    fields = ('name', 'number', 'email', 'location', 'gender',
            'birthday', 'find_us', 'acquaintance_name', 'promo_code',
            'message')


class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'created')
    search_fields = ('email', 'first_name', 'last_name')


class CareerAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'position', 'education_grade')



# admin.site.register(NewsLetter, NewsLetterAdmin)
admin.site.register(Feedback)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Popup)
admin.site.register(LandingContact)
admin.site.register(Career, CareerAdmin)
admin.site.register(Position)

# Add action
admin.site.add_action(export_as_csv_action)
admin.site.add_action(export_as_json_action)
admin.site.add_action(export_as_xml_action)
admin.site.add_action(export_as_yaml_action)
