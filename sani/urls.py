from django.conf.urls import url
from .views import FeedbackCreate, NewsLetterCreate

urlpatterns = [
    url(r"^register-feedback/$", FeedbackCreate.as_view(), name="register-feedback"),
    url(r"^register-newsletter/$", NewsLetterCreate.as_view(), name="register-newsletter"),
]

