from django.db import models
from .validators import validate_file_extension

class NewsLetter(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField()
    created = models.DateTimeField(auto_now_add=True)

    def name(self):
        return u"%s %s" % (self.first_name, self.last_name)

    class Meta:
        unique_together = ('email',)
        ordering = ('-created',)

    def __unicode__(self):
        return u"%s %s" % (self.first_name, self.last_name)


class Feedback(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u"%s - %s" % (self.name, self.email)


class ContactInfoBase(models.Model):
    number = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField()
    location = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    birthday = models.DateField()
    find_us = models.CharField(max_length=255)
    acquaintance_name = models.CharField(max_length=255, blank=True, null=True)
    promo_code = models.CharField(max_length=255, blank=True, null=True)
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Appointment(ContactInfoBase):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)

    class Meta:
        verbose_name='Contact From Web'
        verbose_name_plural='Contact From Web'

    def __unicode__(self):
        return u"%s %s" % (self.first_name, self.last_name)


class Contact(ContactInfoBase):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name='Question From Web'
        verbose_name_plural='Question From Web'

    def __unicode__(self):
        return self.name


class LandingContact(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=30)
    message = models.TextField()
    origin = models.CharField(max_length=50)

    def __unicode__(self):
        return u"%s - %s" % (self.name, self.email)


class Popup(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='popups/')
    small_image = models.ImageField(upload_to='popups/small/')
    url = models.URLField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Position(models.Model):
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class Career(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=255)
    gender = models.CharField(max_length=15)
    birthday = models.DateField()
    education_grade = models.CharField(max_length=255)
    position = models.CharField(max_length=255)
    english = models.IntegerField(default=0, blank=True)
    spanish = models.IntegerField(default=0, blank=True)
    french = models.IntegerField(default=0, blank=True)
    resume = models.FileField(upload_to='carrers/resumes/', blank=True, null=True, validators=[validate_file_extension])

    def __unicode__(self):
        return '%s %s' % (self.name, self.email)

