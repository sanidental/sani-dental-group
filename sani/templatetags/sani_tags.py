from django import template
from mezzanine.pages.models import Page
from sani.models import Popup, Position
from django.utils.safestring import mark_safe
register = template.Library()


@register.simple_tag
def clear_name(name):
    return name.replace('-', ' ')


@register.inclusion_tag('includes/feedback.html')
def render_feedback_popup():
    return {}


@register.simple_tag
def list_positions():
    tag = '<option value="{0}">{0}</option>'
    return mark_safe(''.join([tag.format(x.name) for x in Position.objects.all()]))

