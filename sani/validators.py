import os
from django.core.exceptions import ValidationError


def validate_file_extension(value):
    max_upload_size = 5242880  # 5MB on bytes
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.png']
    if not ext.lower() in valid_extensions and value._size > max_upload_size:
        raise ValidationError(u'Unsupported file extension.')

