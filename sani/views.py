from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from .models import Feedback, NewsLetter


class FeedbackCreate(CreateView):
    model = Feedback
    success_url = reverse_lazy('home')


class NewsLetterCreate(CreateView):
    model = NewsLetter
    success_url = reverse_lazy('home')

    def dispatch(self, request, *args, **kwargs):
        email = request.POST.get('email')
        if NewsLetter.objects.filter(email=email).exists():
            raise Http404
        return super(NewsLetterCreate, self).dispatch(request, *args, **kwargs)
