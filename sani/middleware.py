import hashlib
import logging
import warnings
from django.conf import settings
from django.core import urlresolvers
from django import http
from django.http import HttpResponsePermanentRedirect, HttpResponseGone
from django.utils.http import urlquote
from django.utils import six
from django.middleware.common import BrokenLinkEmailsMiddleware
from django.core.exceptions import MiddlewareNotUsed
from django.contrib.redirects.models import Redirect
from mezzanine.utils.sites import current_site_id

logger = logging.getLogger('django.request')


class RedirectFallbackMiddleware(object):
    """
    Port of Django's ``RedirectFallbackMiddleware`` that uses
    Mezzanine's approach for determining the current site.
    """

    def __init__(self):
        if "django.contrib.redirects" not in settings.INSTALLED_APPS:
            raise MiddlewareNotUsed

    def process_response(self, request, response):
        if response.status_code == 404:
            lookup = {
                "site_id": current_site_id(),
                "old_path": request.get_full_path(),
            }
            try:
                redirect = Redirect.objects.get(**lookup)
            except Redirect.DoesNotExist:
                pass
            else:
                if not redirect.new_path:
                    response = HttpResponseGone()
                else:
                    response = HttpResponsePermanentRedirect(redirect.new_path)
        return response


class SmartAppendSlashMiddleware(object):
    def process_request(self, request):
        """
        Check for denied User-Agents and rewrite the URL based on
        settings.APPEND_SLASH and settings.PREPEND_WWW
        """

        # Check for denied User-Agents
        if 'HTTP_USER_AGENT' in request.META:
            for user_agent_regex in settings.DISALLOWED_USER_AGENTS:
                if user_agent_regex.search(request.META['HTTP_USER_AGENT']):
                    logger.warning('Forbidden (User agent): %s', request.path,
                        extra={
                            'status_code': 403,
                            'request': request
                        }
                    )
                    return http.HttpResponseForbidden('<h1>Forbidden</h1>')

        # Check for a redirect based on settings.SMART_APPEND_SLASH
        # and settings.PREPEND_WWW
        host = request.get_host()
        old_url = [host, request.path]
        new_url = old_url[:]

        if (settings.PREPEND_WWW and old_url[0] and
                not old_url[0].startswith('www.')):
            new_url[0] = 'www.' + old_url[0]

        # Append a slash if SMART_APPEND_SLASH is set and the URL doesn't have a
        # trailing slash and there is no pattern for the current path
        if settings.SMART_APPEND_SLASH and (not old_url[1].endswith('/')):
            for end in settings.SMART_APPEND_SLASH_EXCLUDES:
                if old_url[1].endswith(end):
                    break
            else:
                urlconf = getattr(request, 'urlconf', None)
                if (not urlresolvers.is_valid_path(request.path_info, urlconf) and
                        urlresolvers.is_valid_path("%s/" % request.path_info, urlconf)):
                    new_url[1] = new_url[1] + '/'
                    if settings.DEBUG and request.method == 'POST':
                        raise RuntimeError((""
                        "You called this URL via POST, but the URL doesn't end "
                        "in a slash and you have SMART_APPEND_SLASH set. Django can't "
                        "redirect to the slash URL while maintaining POST data. "
                        "Change your form to point to %s%s (note the trailing "
                        "slash), or set SMART_APPEND_SLASH=False in your Django "
                        "settings.") % (new_url[0], new_url[1]))

        if new_url == old_url:
            # No redirects required.
            return
        if new_url[0]:
            newurl = "%s://%s%s" % (
                'https' if request.is_secure() else 'http',
                new_url[0], urlquote(new_url[1]))
        else:
            newurl = urlquote(new_url[1])
        if request.META.get('QUERY_STRING', ''):
            if six.PY3:
                newurl += '?' + request.META['QUERY_STRING']
            else:
                # `query_string` is a bytestring. Appending it to the unicode
                # string `newurl` will fail if it isn't ASCII-only. This isn't
                # allowed; only broken software generates such query strings.
                # Better drop the invalid query string than crash (#15152).
                try:
                    newurl += '?' + request.META['QUERY_STRING'].decode()
                except UnicodeDecodeError:
                    pass
        return http.HttpResponsePermanentRedirect(newurl)

    def process_response(self, request, response):
        """
        Calculate the ETag, if needed.
        """
        if settings.SEND_BROKEN_LINK_EMAILS:
            warnings.warn("SEND_BROKEN_LINK_EMAILS is deprecated. "
                "Use BrokenLinkEmailsMiddleware instead.",
                PendingDeprecationWarning, stacklevel=2)
            BrokenLinkEmailsMiddleware().process_response(request, response)

        if settings.USE_ETAGS:
            if response.has_header('ETag'):
                etag = response['ETag']
            elif response.streaming:
                etag = None
            else:
                etag = '"%s"' % hashlib.md5(response.content).hexdigest()
            if etag is not None:
                if (200 <= response.status_code < 300
                    and request.META.get('HTTP_IF_NONE_MATCH') == etag):
                    cookies = response.cookies
                    response = http.HttpResponseNotModified()
                    response.cookies = cookies
                else:
                    response['ETag'] = etag

        return response
