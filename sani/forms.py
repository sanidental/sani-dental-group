# -*- encoding: utf-8 -*-
from django import forms
from django.conf import settings
from api.utils import response_to_user, send_to_sani
STATIC_URL = settings.STATIC_URL


class TinyMceWidget(forms.Textarea):
    class Media:
        js = (
            '//cdn.tinymce.com/4/tinymce.min.js',
            "%stinymce.js" % STATIC_URL,
        )

    def __init__(self, *args, **kwargs):
        super(TinyMceWidget, self).__init__(*args, **kwargs)
        self.attrs["class"] = "mceEditor"
