from mezzanine.conf import register_setting

register_setting(
    name="SITE_HOME_DESCRIPTION",
    description="Description tag in homepage.",
    editable=True,
    default="",
)

register_setting(
    name="SITE_HOME_KEYWORDS",
    description="keywords tag in homepage.",
    editable=True,
    default="",
)
