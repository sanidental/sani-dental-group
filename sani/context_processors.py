from django.contrib.sites.models import Site
from mezzanine.conf import settings


def home_settings(request):
    home_url = "//%s" % Site.objects.get_current()
    settings.use_editable()

    return {
        'HOME_URL': home_url,
        'SITE_HOME_DESCRIPTION': settings.SITE_HOME_DESCRIPTION,
        'SITE_HOME_KEYWORDS': settings.SITE_HOME_KEYWORDS,
    }
