# -*- coding: utf-8 -*-
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string


class ProceduresFileWidget(AdminFileWidget):
    input_type = 'text'

    def render(self, name, value, attrs=None):
        template = render_to_string("prices/procedures.html", {
            'lines': value,
            'name': name,
        })
        return mark_safe(template)
