from django import forms
from .models import Quote
from .widgets import ProceduresFileWidget


class QuoteForm(forms.ModelForm):
    procedures = forms.CharField(widget=ProceduresFileWidget)

    class Meta:
        model = Quote
        fields = '__all__'