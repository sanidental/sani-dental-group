import locale
from django import template
from prices.models import Group, Procedure
register = template.Library()


@register.inclusion_tag("prices/group.html")
def render_procedure_groups():
    return {'groups': Group.objects.all()}


@register.inclusion_tag("prices/email_procedures.html")
def procedures_list_email_template(procedures):
    rows = []
    total = long(0)
    for procedure in procedures.split(','):
        id, quantity = procedure.split(':')
        try:
            total += Procedure.objects.get(pk=id).our_price * long(quantity)
            rows.append({
                'procedure': Procedure.objects.get(pk=id),
                'quantity': quantity,
                'total': '{:,}'.format(total)
            })
        except Procedure.DoesNotExist:
            pass
    return {'rows': rows}


@register.inclusion_tag("prices/price-list.html")
def render_price_list():
    return {'groups': Group.objects.all()}
