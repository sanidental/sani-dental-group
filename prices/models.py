from django.db import models
from django.template.defaultfilters import slugify
import unicodedata


class Procedure(models.Model):
    group = models.ForeignKey('Group')
    name = models.CharField(max_length=255)
    our_price = models.IntegerField()
    us_price = models.IntegerField(blank=True, null=True,
                                    verbose_name="US price")
    position = models.IntegerField(default=0)

    class Meta:
        ordering = ('group', 'position', 'name')

    def __unicode__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(max_length=255)
    position = models.IntegerField(default=0)
    slug = models.SlugField(max_length=255, default='')

    class Meta:
        ordering = ('position', 'name')

    def __unicode__(self):
        return self.name

    def procedures(self):
        return Procedure.objects.filter(group=self)

    def save(self, *args, **kwargs):
        self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.name)
                                ) if unicodedata.category(c) != 'Mn'))
        super(Group, self).save(*args, **kwargs)


class Quote(models.Model):
    name = models.CharField(max_length=255)
    telephone = models.CharField(max_length=30)
    email = models.EmailField()
    procedures = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name='Get Free Quote'
        verbose_name_plural='Get Free Quote'


class Treatment(models.Model):
    name = models.CharField(max_length=255)
    procedures = models.ManyToManyField('Procedure', blank=True)
    slug = models.SlugField(max_length=255, default='')

    class Meta:
        ordering = ('name',)

    def __unicode__(self):
        return self.name

    def get_procedures(self):
        return Procedure.objects.filter(treatment=self)

    def save(self, *args, **kwargs):
        self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.name)
                                ) if unicodedata.category(c) != 'Mn'))
        super(Treatment, self).save(*args, **kwargs)
