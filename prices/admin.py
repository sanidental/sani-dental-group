from django.contrib import admin
from .models import Procedure, Group, Quote, Treatment
from .forms import QuoteForm


class ProcedureAdmin(admin.ModelAdmin):
    list_display = ('name', 'our_price', 'us_price', 'position', 'group')
    list_editable = ('our_price', 'us_price', 'position', 'group')
    list_filter = ('group',)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'position')
    readonly_fields = ['slug', ]
    list_editable = ('position',)


class QuoteAdmin(admin.ModelAdmin):
    list_display = ('name', 'telephone', 'email')
    form = QuoteForm


class TreatmentAdmin(admin.ModelAdmin):
    list_display = ('name',)
    filter_horizontal = ('procedures',)
    readonly_fields = ['slug', ]

admin.site.register(Procedure, ProcedureAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Quote, QuoteAdmin)
admin.site.register(Treatment, TreatmentAdmin)
