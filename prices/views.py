from django.views.generic import TemplateView, DetailView
from .models import Treatment


class EmailView(TemplateView):
    template_name = "prices/email_quote.html"

    def get_context_data(self, **kwargs):
        context = super(EmailView, self).get_context_data(**kwargs)
        params = {
            'procedures': '1:1,2:2,3:1,4:2,7:1,17:1,19:1',
            'price': '$1,460',
            'name': 'Jesus Anaya',
            'telephone': '2345643',
            'email': 'jesus.anaya.dev@gmail.com',
        }
        return dict(context.items() + params.items())


class TreatmentDetail(DetailView):
    context_object_name = "groups"

    def get_object(self, queryset=None):
        return None

    def get_template_names(self):
        return 'pages/treatments/'+self.kwargs['slug']+'.html'

    def get_context_data(self, **kwargs):
        context = super(TreatmentDetail, self).get_context_data(**kwargs)
        context['dental-implants'] = Treatment.objects.filter(slug='dental-implants')
        context['dental-implants-supported'] = Treatment.objects.filter(slug='dental-implants-supported')
        context['dentures'] = Treatment.objects.filter(slug='dentures')
        context['veneers'] = Treatment.objects.filter(slug='veneers')
        context['all-on-four'] = Treatment.objects.filter(slug='all-on-four')
        context['full-mouth-restoration'] = Treatment.objects.filter(slug='full-mouth-restoration')
        context['bone-graft-sinus-lift'] = Treatment.objects.filter(slug='bone-graft-sinus-lift')
        context['dental-laser'] = Treatment.objects.filter(slug='dental-laser')
        context['cleaning-whitening'] = Treatment.objects.filter(slug='cleaning-whitening')
        context['crowns'] = Treatment.objects.filter(slug='crowns')
        return context