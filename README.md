# Sani Dental Group
---
### Prerequisites
You will need the following things properly installed on your computer.
- [Git](http://git-scm.com/)
- [Fabric](http://www.fabfile.org/)
- [Node.js](http://nodejs.org/) (with NPM)
- [Virtualenv](https://virtualenv.pypa.io/en/latest/)
- [MySQL](https://www.mysql.com/)
---
### Installation Instructions
- `git clone git@bitbucket.org:sanidental/sani-dental-group.git`
- `cd sani-dental-group`
- `virtualenv .venv`
- `source .venv/bin/activate`
- `pip install -r requirements/develop.txt`
- Create database and config file:
```python
# config/local_settings.py
DEBUG = True
TEMPLATE_DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "STORAGE_ENGINE": "InnoDB",
        "NAME": "your_database_name",
        "USER": "your_database_user",
        "PASSWORD": "your_database_password",
        "HOST": "localhost",
        "PORT": "3306",
    }  
}
```
- Run migrations `python manage.py makemigrations`
- Run server `python manage.py runserver`
- Stop server `ctrl + c`