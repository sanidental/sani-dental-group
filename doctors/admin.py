from config.settings import MEDIA_URL
from django.contrib import admin
from django.utils.safestring import mark_safe
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import Doctor, Study, Credential
from .forms import StudyForm, CredentialForm


class StudyInline(TabularDynamicInlineAdmin):
    model = Study
    form = StudyForm


class CredentialInline(TabularDynamicInlineAdmin):
    model = Credential
    form = CredentialForm


class DoctorAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name',
              'photo', 'university',
              'curriculum_vitae', 'hidden')
    list_display = ('image', 'name', 'position', 'hidden')
    list_editable = ('position', 'hidden')
    inlines = (StudyInline,
               CredentialInline)

    class Media:
        css = {
            'all': ('/static/css/admin/doctor.css',)
        }

    def image(self, obj):
        tag = "<img width='45' height='60' src='"+MEDIA_URL+"{0}'>"
        return mark_safe(tag.format(obj.photo))

admin.site.register(Doctor, DoctorAdmin)
