from django.conf.urls import url
from .views import DoctorProfile


urlpatterns =[
    url(r'^doctors/our-doctors/(?P<slug>[-\w]+)/', DoctorProfile.as_view(), name="doctor"),
]
