from django.db import models
from django.template.defaultfilters import slugify
import unicodedata


class Doctor(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, default='')
    photo = models.ImageField(upload_to='doctors/')
    university = models.CharField(max_length=255)
    curriculum_vitae = models.FileField(upload_to='doctors/',
                                        blank=True, null=True)
    hidden = models.BooleanField(default=False)
    position = models.IntegerField(default=0)

    class Meta:
        ordering = ('position', 'id')

    def __init__(self, *args, **kwargs):
        super(Doctor, self).__init__(*args, **kwargs)
        if self.first_name is not None and self.last_name:
            self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.first_name+" "+self.last_name)
                                ) if unicodedata.category(c) != 'Mn'))

    def save(self, *args, **kwargs):
        self.slug = ''.join((c for c in unicodedata.normalize('NFD',
                                slugify(self.first_name+" "+self.last_name)
                                ) if unicodedata.category(c) != 'Mn'))
        super(Doctor, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name()

    def name(self):
        return u"%s %s" % (self.first_name, self.last_name)

    def studies(self):
        return Study.objects.filter(doctor=self)

    def credentials(self):
        return Credential.objects.filter(doctor=self)


class Study(models.Model):
    doctor = models.ForeignKey('Doctor')
    name = models.CharField(max_length=350)

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Studies'

    def __unicode__(self):
        return self.name


class Credential(models.Model):
    doctor = models.ForeignKey('Doctor')
    credential = models.ImageField(upload_to='doctors/credentials',
                                   blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Credentials'

