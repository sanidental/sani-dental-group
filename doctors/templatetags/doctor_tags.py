from django import template
from doctors.models import Doctor
register = template.Library()


@register.inclusion_tag('doctors/doctors_list.html', takes_context=True)
def render_doctors_list(context):
    context['doctors'] = Doctor.objects.filter(hidden=False)
    return context
