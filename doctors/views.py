from django.views.generic import DetailView
from django.http import Http404
from mezzanine.pages.models import Page
from .models import Doctor


class DoctorProfile(DetailView):
    template_name = "doctors/profile-info.html"
    context_object_name = "doctor"
    model = Doctor

    def get_context_data(self, **kwargs):
        context = super(DoctorProfile, self).get_context_data(**kwargs)
        try:
            context["page"] = Page.objects.get(slug="doctors")
        except Page.DoesNotExist:
            raise Http404
        return context


