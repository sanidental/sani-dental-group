from django import forms
from .models import Study, Credential


class StudyForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'study-name'}),
                           label='Name')

    class Meta:
        model = Study
        fields = '__all__'


class CredentialForm(forms.ModelForm):
    credential = forms.FileField(label='Credential')

    class Meta:
        model = Credential
        fields = ('credential',)
