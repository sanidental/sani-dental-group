from django.db import models


class Restriction(models.Model):
    promotion = models.ForeignKey('Promotion')
    description = models.CharField(max_length=300)

    class Meta:
        ordering = ('-id',)


class Promotion(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    position = models.IntegerField(default=0)

    class Meta:
        ordering = ('position',)

    def __unicode__(self):
        return self.name

    def restrictions(self):
        return Restriction.objects.filter(promotion=self)
