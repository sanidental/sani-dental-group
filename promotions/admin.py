from django.contrib import admin
from django.conf import settings
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import Promotion, Restriction
from .forms import PromotionForm
STATIC_URL = settings.STATIC_URL


class RestrictionInline(TabularDynamicInlineAdmin):
    model = Restriction


class PromotionAdmin(admin.ModelAdmin):
    form = PromotionForm
    inlines = (RestrictionInline,)
    fields = ('name', 'description')
    list_display = ('name', 'position')
    list_editable = ('position',)

    class Media:
          js = (
            '//cdn.tinymce.com/4/tinymce.min.js', 
            "%stinymce.js" % STATIC_URL,
        )


admin.site.register(Promotion, PromotionAdmin)
