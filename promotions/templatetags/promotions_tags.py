from django import template
from promotions.models import Promotion
register = template.Library()


@register.inclusion_tag('promotions/promotions_list.html')
def render_promotions_list():
    return {'promotions': Promotion.objects.all()}
