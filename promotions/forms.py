from django import forms
from .models import Promotion


class PromotionForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'mceEditor'}))

    class Meta:
        model = Promotion
        fields = '__all__'
