from django.conf.urls import include, url
from rest_framework import routers
from .views import (FeedbackViewSet, NewsLetterViewSet,
                    AppointmentViewSet, ContactViewSet,
                    ReviewViewSet, QuoteViewSet,
                    LandingContactViewSet, CarrersViewSet,
                    SelfBookingView,ContactAPIView,
                    FinancingAPIView,FreeQuoteAPIView)


router = routers.DefaultRouter()
router.register(r'feedback', FeedbackViewSet, 'feedback')
router.register(r'newsletter', NewsLetterViewSet, 'newsletter')
router.register(r'appointment', AppointmentViewSet, 'appointment')
router.register(r'contact', ContactViewSet, 'contact')
router.register(r'review', ReviewViewSet, 'review')
router.register(r'quote', QuoteViewSet, 'quote')
router.register(r'landing', LandingContactViewSet, 'landing')
router.register(r'carrers', CarrersViewSet, 'carrers')

urlpatterns = [
    url(r'^selfbooking/$', SelfBookingView.as_view(), name="api-selfbooking"),
    url(r'^contact-APIView/$', ContactAPIView.as_view(), name="contact-APIView"),
    url(r'^financing-APIView/$', FinancingAPIView.as_view(), name="sidebar-financing-view"),
    url(r'^free-quote-APIView/$', FreeQuoteAPIView.as_view(), name="sidebar-free-quote-view"),
    url(r'^', include(router.urls)),
]