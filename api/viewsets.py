from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from django.contrib import messages
from django.shortcuts import HttpResponseRedirect


class CreateOnlyViewSet(mixins.CreateModelMixin, GenericViewSet):
    def pos_create(self, request, filename=''):
        pass

    def dispatch(self, request, *args, **kwargs):
        r = super(CreateOnlyViewSet, self).dispatch(request, *args, **kwargs)

        if r.status_code == 200 or r.status_code == 201:
            if 'resume' in r.data:
                return self.pos_create(request, r.data['resume'])
            else:
                return self.pos_create(request)
        else:
            messages.error(request, 'Error')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))