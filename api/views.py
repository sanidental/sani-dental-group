from django.contrib import messages
from django.shortcuts import redirect, HttpResponseRedirect
from rest_framework.views import APIView
from prices.models import Quote
from reviews.models import Review
from sani.models import (Feedback, NewsLetter,
                         Appointment, Contact,
                         LandingContact, Career)
from .serializers import (FeedbackSerializer, NewsLetterSerializer,
                          AppointmentSerializer, ContactSerializer,
                          ReviewSerializer, QuoteSerializer,
                          LandingContactSerializer, SelfBookingSerializer,
                          FormContactSerializer,FormFinancingSerializer,
                          FormFreeQuoteSerializer, CareerSerializer)
from .utils import response_to_user, send_to_sani
from .viewsets import CreateOnlyViewSet


class AppointmentViewSet(CreateOnlyViewSet):
    model = Appointment
    serializer_class = AppointmentSerializer

    def pos_create(self, request):
        send_to_sani(request, 'send-sani-appointment')


class SelfBookingView(APIView):

    def post(self, request):
        serialized = SelfBookingSerializer(data=request.data)
        if serialized.is_valid(): 
            # response_to_user(request, 'send-sani-selfbooking')
            send_to_sani(request, 'send-sani-selfbooking')
            return redirect('/appointments-thanks/')
        else:
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))


class ContactViewSet(CreateOnlyViewSet):
    model = Contact
    serializer_class = ContactSerializer

    def pos_create(self, request):
        send_to_sani(request, 'send-sani-contact')


class FeedbackViewSet(CreateOnlyViewSet):
    model = Feedback
    serializer_class = FeedbackSerializer

    def pos_create(self, request):
        send_to_sani(request, 'send-sani-feedback')


class NewsLetterViewSet(CreateOnlyViewSet):
    model = NewsLetter
    serializer_class = NewsLetterSerializer

    def pos_create(self, request):
        send_to_sani(request, 'send-sani-newsletter')


class ReviewViewSet(CreateOnlyViewSet):
    model = Review
    serializer_class = ReviewSerializer

    def pos_create(self, request):
        serializer_class = ReviewSerializer(data=request.POST)
        if serializer_class.is_valid():
            send_to_sani(request, 'send-sani-review')
            return redirect('/write-review-thanks/')
        else:
            messages.error(request._request, 'Error')
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))


class QuoteViewSet(CreateOnlyViewSet):
    model = Quote
    serializer_class = QuoteSerializer

    def pos_create(self, request):
        serialized = QuoteSerializer(data=request.POST)
        if serialized.is_valid():
            response_to_user(request, 'send-sani-quote')
            send_to_sani(request, 'send-sani-quote')
            return redirect('/thanks-free-quote')
        else:
            messages.error(request._request, 'Error')
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))


class LandingContactViewSet(CreateOnlyViewSet):
    model = LandingContact
    serializer_class = LandingContactSerializer

    def pos_create(self, request):
        serialized = LandingContactSerializer(data=request.POST)
        if serialized.is_valid():
            send_to_sani(request, 'send-sani-landing')
            return redirect('/landing-thanks/')
        else:
            messages.error(request, 'Error')
            return HttpResponseRedirect(request.environ.get('HTTP_REFERER'))


class CarrersViewSet(CreateOnlyViewSet):
    model = Career
    serializer_class = CareerSerializer
    obj = None

    def pos_create(self, request, filename):
        serialized = CareerSerializer(data=request.POST)
        if serialized.is_valid():
            send_to_sani(request, 'send-sani-carrers', attach='media_root/carrers/resumes/' + filename.split('/')[-1])
            return redirect('/thanks/')
        else:
            messages.error(request, 'Error')
            return HttpResponseRedirect(request.environ.get('HTTP_REFERER'))


class ContactAPIView(APIView):

    def post(self, request):
        serialized = FormContactSerializer(data=request.data)
        if serialized.is_valid():
            send_to_sani(request, 'send-sani-contact')
            return redirect('/thanks/')
        else:
            messages.error(request._request, 'Error')
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))


class FinancingAPIView(APIView):

    def post(self, request):
        serialized = FormFinancingSerializer(data=request.data)
        if serialized.is_valid():
            send_to_sani(request, 'send-sani-financing')
            return redirect('/thanks/')
        else:
            messages.error(request._request, 'Error')
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))


class FreeQuoteAPIView(APIView):

    def post(self, request):
        serialized = FormFreeQuoteSerializer(data=request.data)
        if serialized.is_valid():
            response_to_user(request, 'send-sani-free-quote')
            return redirect('/thanks/')
        else:
            messages.error(request._request, 'Error')
            return HttpResponseRedirect(request._request.environ.get('HTTP_REFERER'))
