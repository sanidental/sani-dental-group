from responses.senders import send_email


def response_to_user(request, token):
    email = request.POST.get('email')
    context = request.POST.dict()

    if email:
        send_email(token, context, email)


def send_to_sani(request, token, attach=None):
    send_email(token, request.POST.dict(), attach=attach)
