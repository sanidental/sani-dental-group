from rest_framework import serializers
from rest_framework.fields import DateField
from sani.models import (Feedback, NewsLetter, Appointment,
                         Contact, LandingContact, Career)
from reviews.models import Review
from prices.models import Quote

DATE_INPUT_FORMATS = ("%m/%d/%Y", "%Y-%m-%d")


class AppointmentSerializer(serializers.ModelSerializer):
    birthday = DateField(input_formats=DATE_INPUT_FORMATS)
    date = DateField(input_formats=DATE_INPUT_FORMATS)

    class Meta:
        model = Appointment
        fields = '__all__'



class SelfBookingSerializer(serializers.Serializer):
    email = serializers.EmailField()
    
    pass


class ContactSerializer(serializers.ModelSerializer):
    birthday = DateField(input_formats=DATE_INPUT_FORMATS)

    class Meta:
        model = Contact
        fields = '__all__'


class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = '__all__'


class NewsLetterSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLetter
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    date = DateField(input_formats=DATE_INPUT_FORMATS)

    class Meta:
        model = Review
        fields = '__all__'


class QuoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quote
        fields = '__all__'


class LandingContactSerializer(serializers.ModelSerializer):
    url = serializers.CharField(allow_blank=True, required=False)

    class Meta:
        model = LandingContact
        fields = '__all__'

    def validate_url(self, value):
        if value and len(value) > 0:
            raise serializers.ValidationError('Spam')
        return value

    def create(self, validated_data):
        data = validated_data.pop('url')
        return LandingContact.objects.create(**validated_data)

class CareerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Career
        fields = '__all__'



class FormContactSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    area = serializers.IntegerField()
    number = serializers.RegexField('[0-9]{3}-[0-9]{4}')
    location = serializers.CharField()
    message = serializers.CharField()


class FormFinancingSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    phone = serializers.CharField()
    residency = serializers.CharField()
    message = serializers.CharField()


class FormFreeQuoteSerializer(serializers.Serializer):
    name = serializers.CharField()
    telephone = serializers.CharField()
    email = serializers.EmailField()
