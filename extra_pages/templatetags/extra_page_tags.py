from django import template
from mezzanine.pages.models import Page
register = template.Library()


@register.inclusion_tag('pages/menus/footer.html')
def render_menu_footer():
    pages = Page.objects.filter(in_menus__contains='3')
    return {'page_branch': pages}
