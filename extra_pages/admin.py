from django.contrib import admin
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from mezzanine.pages.admin import PageAdmin
from mezzanine.pages.models import Page, RichTextPage
from mezzanine.utils.urls import admin_url
from copy import deepcopy
from .models import BlankPage

blankpage_fieldsets = deepcopy(PageAdmin.fieldsets)
blankpage_fieldsets[0][1]["fields"].insert(1, "subtitle")
blankpage_fieldsets[0][1]["fields"].insert(2, "menu_title")
blankpage_fieldsets[0][1]["fields"].insert(4, "use_right_panel")
blankpage_fieldsets[0][1]["fields"].insert(8, "no_link")

page_fieldsets = deepcopy(blankpage_fieldsets)
page_fieldsets[0][1]["fields"].insert(6, "content")


class SaniPageAdminBase(PageAdmin):
    def change_view(self, request, object_id, **kwargs):
        """
        For the ``Page`` model, check ``page.get_content_model()``
        for a subclass and redirect to its admin change view.
        Also enforce custom change permissions for the page instance.
        """
        page = get_object_or_404(Page, pk=object_id)
        content_model = page.get_content_model()
        self.check_permission(request, content_model, "change")
        if self.model is Page:
            if content_model is not None:
                change_url = admin_url(content_model.__class__, "change",
                                       content_model.id)
                return HttpResponseRedirect(change_url)
        kwargs.setdefault("extra_context", {})
        kwargs["extra_context"].update({
            "hide_delete_link": not content_model.can_delete(request),
            "hide_slug_field": False,
        })
        return super(PageAdmin, self).change_view(request, object_id, **kwargs)


class SaniPageAdmin(SaniPageAdminBase):
    fieldsets = page_fieldsets


class BlankPageAdmin(SaniPageAdminBase):
    fieldsets = blankpage_fieldsets


admin.site.unregister(Page)
admin.site.unregister(RichTextPage)
admin.site.register(Page, SaniPageAdmin)
admin.site.register(RichTextPage, SaniPageAdmin)
admin.site.register(BlankPage, BlankPageAdmin)
