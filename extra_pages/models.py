from mezzanine.pages.models import Page


class BlankPage(Page):
    class Meta:
        verbose_name = "Blank Page"
        verbose_name_plural = "Blank Pages"
